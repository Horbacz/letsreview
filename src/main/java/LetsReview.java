import java.util.Scanner;

//https://www.hackerrank.com/challenges/30-review-loop/problem

public class LetsReview {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        Scanner scan = new Scanner(System.in);
        int t = scan.nextInt();
        scan.nextLine();
        String[] s = new String[t];
        for (int y = 0; y < t; y++) {
            s[y] = scan.nextLine();
            if (s[y].length() < 2 || s[y].length() > 10000) {
                throw new IllegalArgumentException("String's length must be between 2 and 10000");
            }
        }
        scan.close();
        if (t >= 1 && t <= 10) {
            for(int i=0;i<t;i++)
            {
                for(int x=0;x<s[i].length();x+=2)
                {
                    System.out.print(s[i].substring(x,x+1));
                }
                System.out.print(" ");
                for(int y=1;y<s[i].length();y+=2)
                {
                    System.out.print(s[i].substring(y,y+1));
                }
                System.out.println();
            }

        }
    }
}



